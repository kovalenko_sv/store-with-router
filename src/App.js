import React, { useEffect, useState } from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Header from "./components/Header/Header";
import Items from "./components/Items/Items";
import ShoppingCart from "./components/ShoppingCart/ShoppingCart";
import Favorites from "./components/Favorites/Favorites";
import Template from "./components/Template/Template";

function App() {
  const [items, setItems] = useState([]);
  const [favorites, setFav] = useState(JSON.parse(localStorage.getItem("favoriteList")) || []);
  const [orders, setOrder] = useState(JSON.parse(localStorage.getItem("productItem")) || []);

  useEffect(() => {
    fetch("./products.json")
      .then((response) => response.json())
      .then((data) => setItems(data));
  }, []);

  const addToOrder = (item) => {
    let newList;
    if (orders.includes(item)) {
      return;
    } else {
      newList = [...orders, item];
    }
    setOrder(newList);
    localStorage.setItem("productItem", JSON.stringify(newList));
  };

  const deleteOrder = (id) => {
    const newCart = orders.filter((el) => el.id !== id);
    setOrder(newCart);
    localStorage.setItem("productItem", JSON.stringify(newCart));
  };

  const addToFav = (item) => {
    let resultArr;
    if (favorites.includes(item.id)) {
      resultArr = favorites.filter((el) => el !== item.id);
    } else {
      resultArr = [...favorites, item.id];
    }
    setFav(resultArr);
    localStorage.setItem("favoriteList", JSON.stringify(resultArr));
  };

  return (
    <div className="App">
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Template />}>
            <Route path='/' index element={<><Header orders={orders} onDelete={deleteOrder} />
              <Items
                items={items}
                onAdd={addToOrder}
                onFav={addToFav}
                favList={favorites}
              /> </>}></Route>
            <Route path='shopping-cart' element={<ShoppingCart orders={orders} onDelete={deleteOrder} />}></Route>
            <Route path='favorites' element={<Favorites items={items} onAdd={addToOrder}
              favList={favorites} onFav={addToFav}
            />}></Route>
          </Route>

        </Routes>
      </BrowserRouter>

    </div>
  );
}

export default App;
