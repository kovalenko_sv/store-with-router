import React, { useState } from "react";
import { Button } from "../Button/Button";
import { Modal, Actions } from "../Modal/Modal";
import { Star } from "../icon/index";
import PropTypes from "prop-types";

export default function Item(props) {
  const [modal, setModal] = useState(false);

  return (
    <div className="item">
      <img src={"./img" + props.item.img} width="200" height="200" />
      <h2>{props.item.title}</h2>
      <p>article: {props.item.article}</p>
      <p>price: {props.item.price}$</p>
      <p>color: {props.item.color}</p>
      <div>
        <Button
          style={{
            backgroundColor: "#0306f6",
            padding: "5px",
            color: "white",
            border: "none",
            borderRadius: "5px",
          }}
          className="first-btn"
          onClick={() => setModal(true)}
          text="Add to cart"
        />
        <Modal
          onModalClose={() => setModal(false)}
          closeBtn={modal}
          header="Do you want to add this product to your cart?"
          text="You can add this item and continue"
          actions={
            <Actions
              onClick={() => {
                props.onAdd(props.item);
                setModal(false);
              }}
              onCancel={() => setModal(false)
              }
              firstBtn="OK"
              secondBtn="CANCEL"
            />
          }
        />
        <button className={`star ${props.isFav ? "active" : ""}`}>
          <Star
            onClick={() => {
              props.onFav(props.item);
            }}
          />
        </button>
      </div>
    </div>
  );
}

Item.propTypes = {
  img: PropTypes.string,
  titile: PropTypes.string,
  article: PropTypes.string,
  color: PropTypes.string,
  price: PropTypes.string,
  id: PropTypes.number,
};
Item.defaultProps = {
  titile: "Title",
  price: "Price",
  article: "0000000",
  color: "black",
};
