import React from "react";
import PropTypes from "prop-types";

export const Star = (props) => {
  return (
    <svg
      onClick={props.onClick}
      xmlns="http://www.w3.org/2000/svg"
      width="13.516"
      height="13.453"
      viewBox="69.695 1.789 13.516 13.453"
    >
      <path
        d="M80.68 15.227c.187-.032-.032-.97-.032-.97l-1.468-3.53c-.47-.626-.032-.97-.032-.97l3.063-1.968c.875-.375 1-.562 1-.562-.375-.47-1.031-.47-1.031-.47h-3.5c-.438 0-.5-.53-.5-.53l-1.032-3.5c-.406-.938-.672-.938-.672-.938h.125s-.437 0-.843.938l-1.032 3.5s-.062.53-.5.53h-3.5s-.656 0-1.03.47c0 0 .155.25 1 .562l3.124 1.969s.5.234-.031.937l-1.5 3.532s-.172.984.016 1.015c.43.025 3.906-3.015 3.906-3.015s.234-.422.516.015c0 0 3.076 2.581 3.953 2.985Z"
        fill="#fff"
        fillRule="evenodd"
        data-name="star"
      />
    </svg>
  );
};

Star.propTypes = {
  onClick: PropTypes.func,
};
