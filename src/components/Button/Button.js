import React from "react";
import PropTypes from "prop-types";

export const Button = (props) => {
  return (
    <button style={props.style} onClick={props.onClick}>
      {props.text}
    </button>
  );
};

Button.propTypes = {
  onClick: PropTypes.func,
  text: PropTypes.string,
};

Button.defaultProps = {
  text: "Add to cart",
};
