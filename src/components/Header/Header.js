import { FaShoppingCart } from "react-icons/fa";
import React, { useState } from "react";
import Order from "../Order/Order";
import PropTypes from "prop-types";

export default function Header(props) {
  let [openCart, setOpenCart] = useState(false);

  return (
    < header >
      {props.orders.length > 0 ? (<p className="summary">You have {props.orders.length} items.</p>) : (<p className="summary">You have 0 items.</p>)}

      <FaShoppingCart
        onClick={() => setOpenCart((openCart = !openCart))}
        className={`shopping-cart ${openCart && "active"}`}
      />
      {
        openCart && (
          <div className="shopping-cart-opened">
            {props.orders.length > 0
              ? showOrders(props)
              : "Your shopping cart is still empty("}
          </div>
        )
      }
    </header >
  );
}

const showOrders = (props) => {
  return (
    <div>
      {props.orders.map((el) => (
        <Order onDelete={props.onDelete} key={el.id} item={el} />
      ))}
    </div>
  );
};

Header.propTypes = {
  summary: PropTypes.number,
  openCart: PropTypes.array,
};

Header.defaultProps = {
  summary: 0,
  openCart: [],
};
