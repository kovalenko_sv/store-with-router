import React from "react";
import Item from "../Item/Item";
import { FaTrash } from "react-icons/fa";
import "./_shopping-cart.scss"

export default function ShoppingCart(props) {
    return (
        <div className="cards-wrap">
            {props.orders.length === 0 ? (<p className="empty-cart">Your shopping cart is empty:(</p>) : null}

            {props.orders.map((el) => (

                <><Item onDelete={props.onDelete} key={el.id} item={el} />
                    <FaTrash
                        className="delete-item"
                        onClick={() => props.onDelete(el.id)}
                    />
                </>

            ))}
        </div>)
}

