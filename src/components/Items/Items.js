import React from "react";
import Item from "../Item/Item";
import PropTypes from "prop-types";

export default function Items(props) {
  return (
    <main>
      {props.items.map((el) => (
        <Item
          key={el.id}
          item={el}
          onAdd={props.onAdd}
          onFav={props.onFav}
          isFav={props.favList.includes(el.id)}
        />
      ))}
    </main>
  );
}

Items.propTypes = {
  id: PropTypes.number,
  onAdd: PropTypes.func,
  onFav: PropTypes.func,
};
