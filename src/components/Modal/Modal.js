import React from "react";
import "./_modal.scss";
import PropTypes from "prop-types";

export const Actions = ({ firstBtn, secondBtn, onClick, onCancel }) => (
  <div className="modal_btns">
    <button className="modal_submit" onClick={onClick}>
      {firstBtn}
    </button>
    <button className="modal_cancel" onClick={onCancel}>
      {secondBtn}
    </button>
  </div>
);

export const Modal = (props) => {
  return (
    <div
      className={`modal_wrapper ${props.closeBtn ? "open" : "close"}`}
      style={{ ...props.style }}
      onClick={(e) => e.currentTarget === e.target && props.onModalClose()}
    >
      <div className="modal_body">
        <div className="modal_header">
          <h2 className="modal_title">{props.header}</h2>
          <button className="modal_close" onClick={props.onModalClose}>
            X
          </button>
        </div>
        <p className="modal_text">{props.text}</p>
        {props.actions}
      </div>
    </div>
  );
};

Modal.propTypes = {
  header: PropTypes.string,
  onModalClose: PropTypes.func,
  text: PropTypes.string,
};
Modal.defaultProps = {
  header: "Do you want to add this product to your cart?",
  text: "You can add this item and continue",
};
Actions.propTypes = {
  firstBtn: PropTypes.string,
  secondBtn: PropTypes.string,
  onCancel: PropTypes.func,
  onClick: PropTypes.func,
};
Actions.defaultProps = {
  firstBtn: "OK",
  secondBtn: "CANCEL",
};
