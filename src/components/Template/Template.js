import React from "react";
import { Outlet, NavLink, Route } from "react-router-dom";
import "./_template.scss";

export default function Template() {
    return (
        <div className="template">
            <nav>
                <NavLink to="/" className={({ isActive }) => isActive ? "active" : ""} >Home</NavLink>
                <NavLink to="favorites">Favorites</NavLink>
                <NavLink to="shopping-cart">Open shopping-cart</NavLink>

            </nav>
            <Outlet />
        </div>
    );
}