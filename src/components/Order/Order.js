import React from "react";
import { FaTrash } from "react-icons/fa";
import PropTypes from "prop-types";


export default function Order(props) {
  return (
    <div className="item">
      <img src={"./img" + props.item.img} width="200" height="200" />
      <h2>{props.item.title}</h2>
      <p>price: {props.item.price}$</p>
      <FaTrash
        className="delete-icon"
        onClick={() => props.onDelete(props.item.id)}
      />
    </div>
  );
}

Order.propTypes = {
  img: PropTypes.string,
  titile: PropTypes.string,
  price: PropTypes.string,
  id: PropTypes.number,
};
Order.defaultProps = {
  titile: "Title",
  price: "Price",
};
