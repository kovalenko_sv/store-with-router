import React from "react";
import Item from "../Item/Item";
import "./_favlist.scss";

export default function Favorites(props) {
    return (
        <div className="cards-wrap">
            {props.items.map((el) => (
                props.favList.includes(el.id) ?
                    (<><Item onDelete={props.onDelete} key={el.id} item={el} onFav={props.onFav}
                        onAdd={props.onAdd}
                        isFav={props.favList.includes(el.id)} />
                    </>) : []

            ))}
        </div>)
}
